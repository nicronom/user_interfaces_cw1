//
// Created by twak on 07/10/2019.
//

#include "responsive_layout.h"
#include <iostream>

using namespace std;

// Constants for consistent margin based on Material Design's recommendations

// at breakpoint range 0-719px
const int kSmallMargin = 16;

// at a breakpoint range 720-1920+px
const int kMargin = 24;

// iterator for label map such that it is not continuously re-declared
map<QString, ResponsiveLabel *>::iterator it;

void ResponsiveLayout::setGeometry(const QRect &r) {

    QLayout::setGeometry(r);

    // stores unique labels passed to Responsive Label
    map<QString, ResponsiveLabel *> label_map;
    // Results cannot be uniquely identified by a map.
    // Hence we use a vector to have a way to reference and access them.
    vector<ResponsiveLabel *> results_list;

    // iterates over labels
    for(int i = 0; i < list_.size(); i++) {
        QLayoutItem *o = list_.at(i);

        try {
            ResponsiveLabel *label = static_cast<ResponsiveLabel *>(o->widget());

            // non-results labels are mapped, results are added to vector list
            if(label->text() != kSearchResult){
                label_map.insert(make_pair(label->text(), label));
            }
            else if(label->text() == kSearchResult){
                results_list.push_back(label);
            }
        }
        catch (bad_cast) {
            cout << "warning, unknown widget class in layout" << endl;
        }
    }

    // the position of y:
    //      below the rendered layout and above the results to be rendered
    int below_layout_y = 0;

    // small device window
    if(r.width() < 812) {
        below_layout_y = renderSmallScreenLayout(r, label_map);
    }

    if(r.width() >= 812) {
        below_layout_y = renderLargeScreenLayout(r, label_map);
    }

    // finally render results
    int below_results_y = renderResults(r, results_list, below_layout_y);
    renderBelowResults(r, label_map, below_results_y);
}

// Renders all labels above the results for a small sized window
int ResponsiveLayout::renderSmallScreenLayout(const QRect &r, map<QString, ResponsiveLabel *> label_map) {

    // parameters for small sized window
    int margin = kSmallMargin;
    int blocks = 4;
    int margin_cut = 2 * margin;
    int width_no_margins = r.width() - margin_cut;
    int height_no_margin = r.height() - margin_cut;
    int block_width = width_no_margins / blocks;
    int block_height = height_no_margin / (blocks * 2);

    setMetricValues(margin, width_no_margins,
                    height_no_margin, block_width, block_height);

    // handlers for this size
    int top_section_y = handleTopSmall(r,label_map);
    int middle_section_y = handleMiddleSmall(r,label_map,top_section_y);

    return middle_section_y;
}

// Renders all labels above the results for a large sized window
int ResponsiveLayout::renderLargeScreenLayout(const QRect &r, map<QString, ResponsiveLabel *> label_map) {

    // parameters for medium sized window
    int margin = kMargin;
    int blocks = 8;
    int margin_cut = 2 * margin;
    int width_no_margins = r.width() - margin_cut;
    int height_no_margin = r.height() - margin_cut;
    int block_width = width_no_margins / blocks;
    int block_height = height_no_margin / (blocks);
    setMetricValues(margin, width_no_margins,
                    height_no_margin, block_width, block_height);

    // handlers for this size
    int top_section_y = handleTopLarge(r,label_map);
    int middle_section_y = handleMiddleLarge(r, label_map, top_section_y);

    return middle_section_y;
}

// Hides the top section of a small sized window
int ResponsiveLayout::handleTopSmall(const QRect &r, map<QString, ResponsiveLabel *> label_map) {

    // calculates appropriate width for each label
    int home_width = metrics_.block_width + metrics_.block_width / 4;
    int basket_width = metrics_.block_width / 1.5;
    int sign_in_width = basket_width;

    // calculates the section's bottom most y position
    int y_pos = metrics_.margin + metrics_.block_height;

    // calculates absolute position to align components
    int basket_x_pos = r.width() - (metrics_.margin + basket_width);
    int sign_in_x_pos = basket_x_pos - sign_in_width;

    // Home link goes top left
    arrangeLabel(label_map, kHomeLink, 0 + metrics_.margin, 0 + metrics_.margin,home_width, metrics_.block_height);

    // Shopping basket and sign-in are glued top right
    arrangeLabel(label_map, kShoppingBasket, basket_x_pos, 0 + metrics_.margin, basket_width, metrics_.block_height);
    arrangeLabel(label_map, kSignIn, sign_in_x_pos, 0 + metrics_.margin, sign_in_width, metrics_.block_height);

    return y_pos;
}

// Handles the top section of a large sized window
int ResponsiveLayout::handleTopLarge(const QRect &r, map<QString, ResponsiveLabel *> label_map) {

    // calculates appropriate width for each label
    int home_width = metrics_.block_width + metrics_.block_width / 4;
    int basket_width = metrics_.block_width / 1.5;
    int sign_in_width = basket_width;

    // calculates the section's bottom most y position
    int y_pos = metrics_.margin + metrics_.block_height;

    // calculates absolute position to align components
    int basket_x_pos = r.width() - (metrics_.margin + basket_width);
    int sign_in_x_pos = basket_x_pos - sign_in_width;

    // Home link goes top left
    arrangeLabel(label_map, kHomeLink, 0 + metrics_.margin, 0 + metrics_.margin,home_width, metrics_.block_height);

    // Shopping basket and sign-in are glued top right
    arrangeLabel(label_map, kShoppingBasket, basket_x_pos, 0 + metrics_.margin, basket_width, metrics_.block_height);
    arrangeLabel(label_map, kSignIn, sign_in_x_pos, 0 + metrics_.margin, sign_in_width, metrics_.block_height);

    return y_pos;
}

// Handles the medium section of a small sized window
int ResponsiveLayout::handleMiddleSmall(const QRect &r, map<QString, ResponsiveLabel *> label_map, int y) {

    // calculates appropriate width for each label
    int search_button_width = metrics_.block_width / 1.5;
    int search_text_width = metrics_.width_no_margins - search_button_width;

    // calculates the section's bottom most y position
    int below_search_y = y + metrics_.margin + metrics_.block_height;
    int below_nav_tab_y = below_search_y + metrics_.margin + metrics_.block_height;
    int y_pos = below_nav_tab_y;

    // calculates absolute position to align components
    int search_text_x_pos = metrics_.margin;
    int search_button_x_pos = search_text_x_pos + search_text_width;

    // labels which are not intended for this size are hidden
    hideLabel(label_map, kSearchOptions);
    hideLabel(label_map, kAdvert);

    // search section labels glued together
    arrangeLabel(label_map, kSearchText, search_text_x_pos, y + metrics_.margin,
                 search_text_width, metrics_.block_height);
    arrangeLabel(label_map, kSearchButton, search_button_x_pos, y + metrics_.margin,
                 search_button_width, metrics_.block_height);

    // navigation panel below the search section
    arrangeLabel(label_map, kNavTabs, 0 + metrics_.margin, below_search_y + metrics_.margin,
                 metrics_.width_no_margins, metrics_.block_height);

    return y_pos;
}

// Handles the middle section of a large sized window
int ResponsiveLayout::handleMiddleLarge(const QRect &r, map<QString, ResponsiveLabel *> label_map, int y) {

    // calculates appropriate width for each label
    int search_opt_width = metrics_.block_width / 1.5;
    int search_button_width = search_opt_width;
    int search_text_width = metrics_.width_no_margins - (search_opt_width + search_button_width);
    int advert_height = metrics_.block_height * 1.2;

    // calculates the section's bottom most y position
    int below_search_y = y + metrics_.margin + metrics_.block_height;
    int below_nav_y = below_search_y + metrics_.margin + metrics_.block_height;
    int y_pos = below_nav_y + metrics_.margin + advert_height;

    // calculates absolute position to align components
    int search_opt_x_pos = metrics_.margin;
    int search_text_x_pos = search_opt_x_pos + search_opt_width;
    int search_button_x_pos = search_text_x_pos + search_text_width;

    // search section labels glued together
    arrangeLabel(label_map, kSearchOptions, 0 + metrics_.margin, y + metrics_.margin,
                 search_opt_width, metrics_.block_height);
    arrangeLabel(label_map, kSearchText, search_text_x_pos, y + metrics_.margin,
                 search_text_width, metrics_.block_height);
    arrangeLabel(label_map, kSearchButton, search_button_x_pos, y + metrics_.margin,
                 search_button_width, metrics_.block_height);

    // navigation panel below the search section
    arrangeLabel(label_map, kNavTabs, 0 + metrics_.margin, below_search_y + metrics_.margin,
                 metrics_.width_no_margins, metrics_.block_height);

    // advert below the navigation panel
    arrangeLabel(label_map, kAdvert, 0 + metrics_.margin, below_nav_y + metrics_.margin,
                 metrics_.width_no_margins, advert_height);
    // height is too small to render an advert, therefore hides it
    if (r.height() < 720) {
        hideLabel(label_map,kAdvert);
        y_pos = y_pos - (advert_height + metrics_.margin);
    }

    return y_pos;
}

// Renders the results appropriate to the current window size.
// Hides results which cannot be displayed.
int ResponsiveLayout::renderResults(const QRect &r, vector<ResponsiveLabel *> results, int y) {
    // maximum results to display, depending on window size
    int max_results = 2;
    if (r.width() >= 1000) {
        max_results = 4;
    }
    // the amount of result labels does not exceed the maximum allowed for window size
    if (results.size() < max_results) {
        max_results = results.size(); // this will ensure logic below works as intended
    }
    // small width x large height displays results vertically
    bool in_line_vertical = false;
    if  (r.height() >= 720 && max_results == 2) {
        in_line_vertical = true;
    }
    // calculate the metrics of each result to render
    int result_height = determineResultHeight(r,y);
    int result_width = (metrics_.width_no_margins - ((max_results-1)*metrics_.margin)) / max_results;
    // fix the metrics appropriately when results are rendered vertically
    if (in_line_vertical) {
        result_height = (result_height - ((max_results-1)*metrics_.margin)) / 2;
        result_width = metrics_.width_no_margins;
    }
    // variables to determine where the result is rendered in the below loop
    int at_x = 0 + metrics_.margin;
    int at_y = y + metrics_.margin;
    int at_result = 0;
    // render the results to be displayed at this window size
    while (at_x < r.width() - metrics_.margin && at_result < max_results) {
        results[at_result]->setGeometry(at_x, at_y, result_width, result_height);
        // adjust y position if results are being displayed vertically
        if (in_line_vertical) {
            at_y = at_y + result_height + metrics_.margin;
        }
        // horizontally displayed results, adjust x position
        else if (!in_line_vertical) {
            at_x = at_x + result_width + metrics_.margin;
        }
        at_result++;
    }
    // hide results which cannot be rendered for this window size
    while (at_result >= max_results && at_result < results.size()) {
        results[at_result]->setGeometry(0,0,0,0);
        at_result++;
    }
    if (!in_line_vertical) { // y position for horizontal mode was not accounted for
        at_y = at_y + result_height + metrics_.margin;
    }
    return at_y - metrics_.margin;
}

// Determines what the height of a result label should be,
// depending on what is rendered in the section below results
int ResponsiveLayout::determineResultHeight(const QRect &r, int y) {

    // the height before the next labels are accounted for
    int height_remaining = metrics_.height_no_margin - y;
    // the height of labels rendered in the section below results
    int search_arrow_height = 2*metrics_.margin;
    // note that margins get larger on 812 width,
    // hence adjust the search arrow to not take too much height
    if (r.width() >= 812 && r.height() <= 480) {
        search_arrow_height = metrics_.margin / 1.5;
    }
    int next_section_height = search_arrow_height + metrics_.margin;

    // header will be included on windows on large heights, adjust results height
    if (r.height() >= 812) {
        int footer_height = r.height() / 12;
        next_section_height = next_section_height + footer_height;
    }

    // finally calculate the remaining height for a result label
    return height_remaining - next_section_height;
}

// Renders the search page arrows and if the height allows a footer as well
int ResponsiveLayout::renderBelowResults(const QRect &r, map<QString, ResponsiveLabel *> label_map, int y) {

    // metrics for the search arrows
    int search_arrow_width = 3*metrics_.margin;
    int search_arrow_height = 2*metrics_.margin;
    // note that margins get larger on 812 width,
    // hence adjust the search arrow to not take too much height
    if (r.width() >= 812 && r.height() <= 480) {
        search_arrow_height = metrics_.margin / 1.5;
    }

    // centralise the position of the arrows
    int search_left_arrow_x = 0 + ((r.width() - metrics_.margin) / 2 - (search_arrow_width));
    int search_right_arrow_x = search_left_arrow_x + search_arrow_width + metrics_.margin;
    int search_arrow_y = y + metrics_.margin;

    // arranges the search arrows with the assigned metrics and centralised position
    arrangeLabel(label_map, kSearchBackward, search_left_arrow_x, y + metrics_.margin,
                 search_arrow_width, search_arrow_height);
    arrangeLabel(label_map, kSearchForward, search_right_arrow_x, search_arrow_y,
                 search_arrow_width, search_arrow_height);

    // calculates the y which can be used to position a footer if height permits
    int below_arrows_y = search_arrow_y + search_arrow_height + metrics_.margin;

    // positions the footer
    int footer_height = r.height() / 12;
    arrangeLabel(label_map, kFooter, 0, below_arrows_y, r.width(), footer_height);

    // footer is hidden when height is below 812
    if (r.height() < 812) {
        hideLabel(label_map, kFooter);
    }
}

// Given a map of labels and the label to be arranged, sets the label to the given coordinates.
void ResponsiveLayout::arrangeLabel(map<QString, ResponsiveLabel *> label_map, QString label,
                                    int x, int y, int width, int height) {

    it = label_map.find(label);
    if (it != label_map.end()) {
        it->second->setGeometry(x, y, width, height);
    }
}

// Hides the appropriate label, given a label map
void ResponsiveLayout::hideLabel(map<QString, ResponsiveLabel *> label_map, QString label) {

    it = label_map.find(label);
    if (it != label_map.end()) {
        it->second->setGeometry(0,0,0,0);
    }
}

// Sets private struct field to consistent metrics for the appropriate window size.
// That way class methods can access these values to arrange labels accordingly.
void ResponsiveLayout::setMetricValues(int margin,
                                       int width_no_margins,
                                       int height_no_margin,
                                       int block_width,
                                       int block_height) {

    metrics_.margin = margin;
    metrics_.width_no_margins = width_no_margins;
    metrics_.height_no_margin = height_no_margin;
    metrics_.block_width = block_width;
    metrics_.block_height = block_height;
}

// following methods provide a trivial list-based implementation of the QLayout class
int ResponsiveLayout::count() const {
    return list_.size();
}

QLayoutItem *ResponsiveLayout::itemAt(int idx) const {
    return list_.value(idx);
}

QLayoutItem *ResponsiveLayout::takeAt(int idx) {
    return idx >= 0 && idx < list_.size() ? list_.takeAt(idx) : 0;
}

void ResponsiveLayout::addItem(QLayoutItem *item) {
    list_.append(item);
}

QSize ResponsiveLayout::sizeHint() const {
    return minimumSize();
}

QSize ResponsiveLayout::minimumSize() const {
    return QSize(320,320);
}

ResponsiveLayout::~ResponsiveLayout() {
    QLayoutItem *item;
    while ((item = takeAt(0)))
        delete item;
}
