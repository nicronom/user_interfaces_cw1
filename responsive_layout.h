//
// Created by twak on 07/10/2019.
//

#ifndef RESPONSIVELAYOUT_H
#define RESPONSIVELAYOUT_H

#include <QtGui>
#include <QList>
#include <QLayout>
#include <responsive_label.h>

class ResponsiveLayout : public QLayout
{
public:

    ResponsiveLayout(): QLayout() {}
    ~ResponsiveLayout();

    // Standard functions for a QLayout
    void setGeometry(const QRect &rect);
    void addItem(QLayoutItem *item);
    QSize sizeHint() const;
    QSize minimumSize() const;
    int count() const;
    QLayoutItem *itemAt(int) const;
    QLayoutItem *takeAt(int);

    // Functions to handle size and arrangements across all window sizes.
    void
    setMetricValues(int margin, int width_no_margins,
                    int height_no_margin, int block_width, int block_height);
    void arrangeLabel(map<QString, ResponsiveLabel *> label_map, QString label,
                      int x, int y, int width, int height);
    void hideLabel(map<QString, ResponsiveLabel *> label_map, QString label);

    // Functions to render the layout for a particular window size
    int renderSmallScreenLayout(const QRect &rect, map<QString, ResponsiveLabel *> label_map);
    int handleTopSmall(const QRect &r, map<QString, ResponsiveLabel *> labelMap);
    int handleMiddleSmall(const QRect &r, map<QString, ResponsiveLabel *> label_map, int y);

    int renderLargeScreenLayout(const QRect &r, map<QString, ResponsiveLabel *> label_map);
    int handleTopLarge(const QRect &r, map<QString, ResponsiveLabel *> label_map);
    int handleMiddleLarge(const QRect &r, map<QString, ResponsiveLabel *> label_map, int y);

    // Function to render the results below the appropriately rendered layout
    int renderResults(const QRect &r, vector<ResponsiveLabel *> results, int y);
    int determineResultHeight(const QRect &r, int y);
    int renderBelowResults(const QRect &r, map<QString, ResponsiveLabel *> label_map, int y);

private:
    QList<QLayoutItem*> list_;

    // block of constant values that define the metrics of current window
    typedef struct LayoutMetrics {

        int margin;
        int width_no_margins;
        int height_no_margin;
        int block_width;
        int block_height;

    } Metrics;

    // modified according to the size of window
    Metrics metrics_;

};
#endif // RESPONSIVELAYOUT_H
